package cn.anline.qqmap.area.pull.controller;

import act.db.ebean.EbeanDao;
import cn.anline.qqmap.area.pull.entity.Ann_area;
import cn.anline.qqmap.area.pull.pojo.ResponseDTO;
import cn.anline.qqmap.area.pull.pojo.ResultDTO;
import com.google.gson.Gson;
import okhttp3.*;
import org.osgl.mvc.annotation.GetAction;

import javax.inject.Inject;

import java.io.IOException;
import java.util.function.Consumer;

import static act.controller.Controller.Util.*;

public class Home {

    final String QQMAP_KEY = "腾讯地图 webService api Key";

    @Inject
    EbeanDao<Long, Ann_area> annAreaEbeanDao;

    @GetAction
    public Object index(){
        Iterable<Ann_area> annAreaList = annAreaEbeanDao.findBy("level_type",3);

        annAreaList.forEach(new Consumer<Ann_area>() {
            @Override
            public void accept(final Ann_area ann_area) {
                OkHttpClient httpClient = new OkHttpClient();
                String url = "https://apis.map.qq.com/ws/district/v1/getchildren"+"?id="+ann_area.getId()+"&key="+QQMAP_KEY;
                Request request = new Request.Builder()
                        .url(url)
                        .build();
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    System.out.println("线程Sleep");
                    e.printStackTrace();
                }
                httpClient.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        System.out.println("OKhttp请求失败");
                        e.printStackTrace();
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        String res = response.body().string();
                        Gson gson = new Gson();
                        ResponseDTO responseDTO = gson.fromJson(res, ResponseDTO.class);
                        if (responseDTO.getStatus() == 0) {
//                            System.out.println("请求腾讯接口成功");
//                            System.out.println(responseDTO.toString());
                            if (responseDTO.getResult().size() > 0){
                                for (ResultDTO resultDTO: responseDTO.getResult().get(0)){
//                                    检查id是否存在
                                    Ann_area ann_area1 = annAreaEbeanDao.findById(resultDTO.getId());
                                    if (ann_area1  == null){
                                        Ann_area ann_area2 = new Ann_area();
                                        ann_area2.setId(resultDTO.getId());
                                        ann_area2.setPid(ann_area.getId());
                                        ann_area2.setName(resultDTO.getFullname());
                                        ann_area2.setLat(resultDTO.getLocation().getLat());
                                        ann_area2.setLng(resultDTO.getLocation().getLng());
                                        ann_area2.setLevel_type(4L);
                                        ann_area2.setCity_code(ann_area.getCity_code());
                                        ann_area2.setMerger_name(ann_area.getMerger_name() + "," + resultDTO.getFullname());
                                        ann_area2.setData_version(responseDTO.getDataVersion());
                                        Ann_area ann_area3 = annAreaEbeanDao.save(ann_area2);
                                        if (ann_area3 == null){
                                            System.out.println("数据库添加数据失败");
                                            System.out.println(ann_area2);
                                        }
                                    }else {
                                        System.out.println("数据库已存在");
                                        System.out.println(ann_area1);
                                    }

                                }
                            }

                        }else {
                            System.out.println("腾讯接口请求失败");
                            System.out.println(responseDTO.toString());
                        }
                    }
                });
            }
        });
        return json(annAreaList);
    }
}
