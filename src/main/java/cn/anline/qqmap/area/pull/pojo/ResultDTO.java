package cn.anline.qqmap.area.pull.pojo;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

@Generated("com.robohorse.robopojogenerator")
public class ResultDTO implements Serializable {

	@SerializedName("id")
	private Long id;

	@SerializedName("fullname")
	private String fullname;

	@SerializedName("location")
	private LocationDTO location;

	public void setId(Long id){
		this.id = id;
	}

	public Long getId(){
		return id;
	}

	public void setFullname(String fullname){
		this.fullname = fullname;
	}

	public String getFullname(){
		return fullname;
	}

	public void setLocation(LocationDTO location){
		this.location = location;
	}

	public LocationDTO getLocation(){
		return location;
	}

	@Override
 	public String toString(){
		return 
			"ResultDTO{" + 
			"id = '" + id + '\'' + 
			",fullname = '" + fullname + '\'' + 
			",location = '" + location + '\'' + 
			"}";
		}
}