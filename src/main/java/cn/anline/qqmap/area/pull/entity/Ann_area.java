package cn.anline.qqmap.area.pull.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Ann_area {

  @Id
  private Long id;
  private Long pid;
  private String name;
  private String merger_name;
  private String short_name;
  private String merger_short_name;
  private Long level_type;
  private Long city_code;
  private Long zip_code;
  private String pinyin;
  private String jianpin;
  private String first_char;
  private String lng;
  private String lat;
  private String remark;
  private Long create_time;
  private Long update_time;
  private String data_version;


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public Long getPid() {
    return pid;
  }

  public void setPid(Long pid) {
    this.pid = pid;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public String getMerger_name() {
    return merger_name;
  }

  public void setMerger_name(String merger_name) {
    this.merger_name = merger_name;
  }


  public String getShort_name() {
    return short_name;
  }

  public void setShort_name(String short_name) {
    this.short_name = short_name;
  }


  public String getMerger_short_name() {
    return merger_short_name;
  }

  public void setMerger_short_name(String merger_short_name) {
    this.merger_short_name = merger_short_name;
  }


  public Long getLevel_type() {
    return level_type;
  }

  public void setLevel_type(Long level_type) {
    this.level_type = level_type;
  }


  public Long getCity_code() {
    return city_code;
  }

  public void setCity_code(Long city_code) {
    this.city_code = city_code;
  }


  public Long getZip_code() {
    return zip_code;
  }

  public void setZip_code(Long zip_code) {
    this.zip_code = zip_code;
  }


  public String getPinyin() {
    return pinyin;
  }

  public void setPinyin(String pinyin) {
    this.pinyin = pinyin;
  }


  public String getJianpin() {
    return jianpin;
  }

  public void setJianpin(String jianpin) {
    this.jianpin = jianpin;
  }


  public String getFirst_char() {
    return first_char;
  }

  public void setFirst_char(String first_char) {
    this.first_char = first_char;
  }


  public String getLng() {
    return lng;
  }

  public void setLng(String lng) {
    this.lng = lng;
  }


  public String getLat() {
    return lat;
  }

  public void setLat(String lat) {
    this.lat = lat;
  }


  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }


  public Long getCreate_time() {
    return create_time;
  }

  public void setCreate_time(Long create_time) {
    this.create_time = create_time;
  }


  public Long getUpdate_time() {
    return update_time;
  }

  public void setUpdate_time(Long update_time) {
    this.update_time = update_time;
  }


  public String getData_version() {
    return data_version;
  }

  public void setData_version(String data_version) {
    this.data_version = data_version;
  }

}
