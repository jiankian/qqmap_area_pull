package cn.anline.qqmap.area.pull.pojo;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

@Generated("com.robohorse.robopojogenerator")
public class ResponseDTO implements Serializable {

	@SerializedName("status")
	private int status;

	@SerializedName("message")
	private String message;

	@SerializedName("data_version")
	private String dataVersion;

	@SerializedName("result")
	private List<List<ResultDTO>> result;

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setDataVersion(String dataVersion){
		this.dataVersion = dataVersion;
	}

	public String getDataVersion(){
		return dataVersion;
	}

	public void setResult(List<List<ResultDTO>> result){
		this.result = result;
	}

	public List<List<ResultDTO>> getResult(){
		return result;
	}

	@Override
 	public String toString(){
		return 
			"ResponseDTO{" + 
			"status = '" + status + '\'' + 
			",message = '" + message + '\'' + 
			",data_version = '" + dataVersion + '\'' + 
			",result = '" + result + '\'' + 
			"}";
		}
}