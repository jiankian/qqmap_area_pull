package cn.anline.qqmap.area.pull.pojo;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

@Generated("com.robohorse.robopojogenerator")
public class LocationDTO implements Serializable {

	@SerializedName("lat")
	private String lat;

	@SerializedName("lng")
	private String lng;

	public void setLat(String lat){
		this.lat = lat;
	}

	public String getLat(){
		return lat;
	}

	public void setLng(String lng){
		this.lng = lng;
	}

	public String getLng(){
		return lng;
	}

	@Override
 	public String toString(){
		return 
			"LocationDTO{" + 
			"lat = '" + lat + '\'' + 
			",lng = '" + lng + '\'' + 
			"}";
		}
}