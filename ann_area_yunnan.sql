/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 50723
 Source Host           : localhost:3306
 Source Schema         : bfxn_serve

 Target Server Type    : MySQL
 Target Server Version : 50723
 File Encoding         : 65001

 Date: 26/09/2018 17:21:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ann_area
-- ----------------------------
DROP TABLE IF EXISTS `ann_area`;
CREATE TABLE `ann_area` (
  `id` int(45) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id adcode',
  `pid` int(45) DEFAULT NULL COMMENT '父级adcode',
  `name` varchar(45) DEFAULT NULL COMMENT '名称',
  `merger_name` varchar(200) DEFAULT NULL COMMENT '合并',
  `short_name` varchar(45) DEFAULT NULL COMMENT '简称',
  `merger_short_name` varchar(200) DEFAULT NULL COMMENT '简称合并',
  `level_type` int(45) DEFAULT NULL COMMENT '级别',
  `city_code` int(45) DEFAULT NULL COMMENT '区号',
  `zip_code` int(45) DEFAULT NULL COMMENT '邮编',
  `pinyin` varchar(45) DEFAULT NULL COMMENT '拼音',
  `jianpin` varchar(45) DEFAULT NULL COMMENT '字母',
  `first_char` varchar(45) DEFAULT NULL COMMENT '首字母',
  `lng` varchar(45) DEFAULT NULL COMMENT '经度',
  `lat` varchar(45) DEFAULT NULL COMMENT '纬度',
  `remark` varchar(45) DEFAULT NULL COMMENT '别名',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=900001 DEFAULT CHARSET=utf8mb4 COMMENT='省市区县 区域数据';

-- ----------------------------
-- Records of ann_area
-- ----------------------------
BEGIN;
INSERT INTO `ann_area` VALUES (100000, 0, '中国', '中国', '中国', '中国', 0, NULL, NULL, 'China', 'CN', 'C', '116.3683244', '39.915085', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530000, 100000, '云南省', '中国,云南省', '云南', '中国,云南', 1, NULL, NULL, 'Yunnan', 'YN', 'Y', '102.712251', '25.040609', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530100, 530000, '昆明市', '中国,云南省,昆明市', '昆明', '中国,云南,昆明', 2, 871, 650000, 'Kunming', 'KM', 'K', '102.712251', '25.040609', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530102, 530100, '五华区', '中国,云南省,昆明市,五华区', '五华', '中国,云南,昆明,五华', 3, 871, 650000, 'Wuhua', 'WH', 'W', '102.70786', '25.03521', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530103, 530100, '盘龙区', '中国,云南省,昆明市,盘龙区', '盘龙', '中国,云南,昆明,盘龙', 3, 871, 650000, 'Panlong', 'PL', 'P', '102.71994', '25.04053', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530111, 530100, '官渡区', '中国,云南省,昆明市,官渡区', '官渡', '中国,云南,昆明,官渡', 3, 871, 650200, 'Guandu', 'GD', 'G', '102.74362', '25.01497', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530112, 530100, '西山区', '中国,云南省,昆明市,西山区', '西山', '中国,云南,昆明,西山', 3, 871, 650100, 'Xishan', 'XS', 'X', '102.66464', '25.03796', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530113, 530100, '东川区', '中国,云南省,昆明市,东川区', '东川', '中国,云南,昆明,东川', 3, 871, 654100, 'Dongchuan', 'DC', 'D', '103.18832', '26.083', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530114, 530100, '呈贡区', '中国,云南省,昆明市,呈贡区', '呈贡', '中国,云南,昆明,呈贡', 3, 871, 650500, 'Chenggong', 'CG', 'C', '102.801382', '24.889275', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530115, 530100, '晋宁区', '中国,云南省,昆明市,晋宁区', '晋宁', '中国,云南,昆明,晋宁', 3, 871, 650600, 'Jinning', 'JN', 'J', '102.59393', '24.6665', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530124, 530100, '富民县', '中国,云南省,昆明市,富民县', '富民', '中国,云南,昆明,富民', 3, 871, 650400, 'Fumin', 'FM', 'F', '102.4985', '25.22119', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530125, 530100, '宜良县', '中国,云南省,昆明市,宜良县', '宜良', '中国,云南,昆明,宜良', 3, 871, 652100, 'Yiliang', 'YL', 'Y', '103.14117', '24.91705', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530126, 530100, '石林彝族自治县', '中国,云南省,昆明市,石林彝族自治县', '石林', '中国,云南,昆明,石林', 3, 871, 652200, 'Shilin', 'SL', 'S', '103.27148', '24.75897', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530127, 530100, '嵩明县', '中国,云南省,昆明市,嵩明县', '嵩明', '中国,云南,昆明,嵩明', 3, 871, 651700, 'Songming', 'SM', 'S', '103.03729', '25.33986', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530128, 530100, '禄劝彝族苗族自治县', '中国,云南省,昆明市,禄劝彝族苗族自治县', '禄劝', '中国,云南,昆明,禄劝', 3, 871, 651500, 'Luquan', 'LQ', 'L', '102.4671', '25.55387', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530129, 530100, '寻甸回族彝族自治县 ', '中国,云南省,昆明市,寻甸回族彝族自治县 ', '寻甸', '中国,云南,昆明,寻甸', 3, 871, 655200, 'Xundian', 'XD', 'X', '103.2557', '25.55961', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530181, 530100, '安宁市', '中国,云南省,昆明市,安宁市', '安宁', '中国,云南,昆明,安宁', 3, 871, 650300, 'Anning', 'AN', 'A', '102.46972', '24.91652', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530182, 530100, '滇中新区', '中国,云南省,昆明市,滇中新区', '滇中新区', '中国,云南,昆明,滇中新区', 3, 871, 650000, 'DianZhongXinQu', 'DZXQ', 'D', '102.712251', '25.040609', '国家级新区', NULL, NULL);
INSERT INTO `ann_area` VALUES (530183, 530100, '高新区', '中国,云南省,昆明市,高新区', '高新区', '中国,云南,昆明,高新区', 3, 871, 650000, 'Gaoxinqu', 'GXQ', 'G', '102.653544', '25.072794', '高新技术开发区', NULL, NULL);
INSERT INTO `ann_area` VALUES (530300, 530000, '曲靖市', '中国,云南省,曲靖市', '曲靖', '中国,云南,曲靖', 2, 874, 655000, 'Qujing', 'QJ', 'Q', '103.797851', '25.501557', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530302, 530300, '麒麟区', '中国,云南省,曲靖市,麒麟区', '麒麟', '中国,云南,曲靖,麒麟', 3, 874, 655000, 'Qilin', 'QL', 'Q', '103.80504', '25.49515', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530303, 530300, '沾益区', '中国,云南省,曲靖市,沾益区', '沾益', '中国,云南,曲靖,沾益', 3, 874, 655331, 'Zhanyi', 'ZY', 'Z', '103.82135', '25.60715', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530321, 530300, '马龙县', '中国,云南省,曲靖市,马龙县', '马龙', '中国,云南,曲靖,马龙', 3, 874, 655100, 'Malong', 'ML', 'M', '103.57873', '25.42521', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530322, 530300, '陆良县', '中国,云南省,曲靖市,陆良县', '陆良', '中国,云南,曲靖,陆良', 3, 874, 655600, 'Luliang', 'LL', 'L', '103.6665', '25.02335', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530323, 530300, '师宗县', '中国,云南省,曲靖市,师宗县', '师宗', '中国,云南,曲靖,师宗', 3, 874, 655700, 'Shizong', 'SZ', 'S', '103.99084', '24.82822', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530324, 530300, '罗平县', '中国,云南省,曲靖市,罗平县', '罗平', '中国,云南,曲靖,罗平', 3, 874, 655800, 'Luoping', 'LP', 'L', '104.30859', '24.88444', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530325, 530300, '富源县', '中国,云南省,曲靖市,富源县', '富源', '中国,云南,曲靖,富源', 3, 874, 655500, 'Fuyuan', 'FY', 'F', '104.25387', '25.66587', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530326, 530300, '会泽县', '中国,云南省,曲靖市,会泽县', '会泽', '中国,云南,曲靖,会泽', 3, 874, 654200, 'Huize', 'HZ', 'H', '103.30017', '26.41076', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530381, 530300, '宣威市', '中国,云南省,曲靖市,宣威市', '宣威', '中国,云南,曲靖,宣威', 3, 874, 655400, 'Xuanwei', 'XW', 'X', '104.10409', '26.2173', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530400, 530000, '玉溪市', '中国,云南省,玉溪市', '玉溪', '中国,云南,玉溪', 2, 877, 653100, 'Yuxi', 'YX', 'Y', '102.543907', '24.350461', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530402, 530400, '红塔区', '中国,云南省,玉溪市,红塔区', '红塔', '中国,云南,玉溪,红塔', 3, 877, 653100, 'Hongta', 'HT', 'H', '102.5449', '24.35411', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530403, 530400, '江川区', '中国,云南省,玉溪市,江川区', '江川', '中国,云南,玉溪,江川', 3, 877, 652600, 'Jiangchuan', 'JC', 'J', '102.75412', '24.28863', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530422, 530400, '澄江县', '中国,云南省,玉溪市,澄江县', '澄江', '中国,云南,玉溪,澄江', 3, 877, 652500, 'Chengjiang', 'CJ', 'C', '102.90817', '24.67376', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530423, 530400, '通海县', '中国,云南省,玉溪市,通海县', '通海', '中国,云南,玉溪,通海', 3, 877, 652700, 'Tonghai', 'TH', 'T', '102.76641', '24.11362', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530424, 530400, '华宁县', '中国,云南省,玉溪市,华宁县', '华宁', '中国,云南,玉溪,华宁', 3, 877, 652800, 'Huaning', 'HN', 'H', '102.92831', '24.1926', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530425, 530400, '易门县', '中国,云南省,玉溪市,易门县', '易门', '中国,云南,玉溪,易门', 3, 877, 651100, 'Yimen', 'YM', 'Y', '102.16354', '24.67122', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530426, 530400, '峨山彝族自治县', '中国,云南省,玉溪市,峨山彝族自治县', '峨山', '中国,云南,玉溪,峨山', 3, 877, 653200, 'Eshan', 'ES', 'E', '102.40576', '24.16904', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530427, 530400, '新平彝族傣族自治县', '中国,云南省,玉溪市,新平彝族傣族自治县', '新平', '中国,云南,玉溪,新平', 3, 877, 653400, 'Xinping', 'XP', 'X', '101.98895', '24.06886', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530428, 530400, '元江哈尼族彝族傣族自治县', '中国,云南省,玉溪市,元江哈尼族彝族傣族自治县', '元江', '中国,云南,玉溪,元江', 3, 877, 653300, 'Yuanjiang', 'YJ', 'Y', '101.99812', '23.59655', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530500, 530000, '保山市', '中国,云南省,保山市', '保山', '中国,云南,保山', 2, 875, 678000, 'Baoshan', 'BS', 'B', '99.167133', '25.111802', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530502, 530500, '隆阳区', '中国,云南省,保山市,隆阳区', '隆阳', '中国,云南,保山,隆阳', 3, 875, 678000, 'Longyang', 'LY', 'L', '99.16334', '25.11163', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530521, 530500, '施甸县', '中国,云南省,保山市,施甸县', '施甸', '中国,云南,保山,施甸', 3, 875, 678200, 'Shidian', 'SD', 'S', '99.18768', '24.72418', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530523, 530500, '龙陵县', '中国,云南省,保山市,龙陵县', '龙陵', '中国,云南,保山,龙陵', 3, 875, 678300, 'Longling', 'LL', 'L', '98.69024', '24.58746', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530524, 530500, '昌宁县', '中国,云南省,保山市,昌宁县', '昌宁', '中国,云南,保山,昌宁', 3, 875, 678100, 'Changning', 'CN', 'C', '99.6036', '24.82763', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530581, 530500, '腾冲市', '中国,云南省,保山市,腾冲市', '腾冲', '中国,云南,保山,腾冲', 3, 875, 679100, 'Tengchong', 'TC', 'T', '98.49414', '25.02539', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530600, 530000, '昭通市', '中国,云南省,昭通市', '昭通', '中国,云南,昭通', 2, 870, 657000, 'Zhaotong', 'ZT', 'Z', '103.717216', '27.336999', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530602, 530600, '昭阳区', '中国,云南省,昭通市,昭阳区', '昭阳', '中国,云南,昭通,昭阳', 3, 870, 657000, 'Zhaoyang', 'ZY', 'Z', '103.70654', '27.31998', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530621, 530600, '鲁甸县', '中国,云南省,昭通市,鲁甸县', '鲁甸', '中国,云南,昭通,鲁甸', 3, 870, 657100, 'Ludian', 'LD', 'L', '103.54721', '27.19238', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530622, 530600, '巧家县', '中国,云南省,昭通市,巧家县', '巧家', '中国,云南,昭通,巧家', 3, 870, 654600, 'Qiaojia', 'QJ', 'Q', '102.92416', '26.91237', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530623, 530600, '盐津县', '中国,云南省,昭通市,盐津县', '盐津', '中国,云南,昭通,盐津', 3, 870, 657500, 'Yanjin', 'YJ', 'Y', '104.23461', '28.10856', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530624, 530600, '大关县', '中国,云南省,昭通市,大关县', '大关', '中国,云南,昭通,大关', 3, 870, 657400, 'Daguan', 'DG', 'D', '103.89254', '27.7488', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530625, 530600, '永善县', '中国,云南省,昭通市,永善县', '永善', '中国,云南,昭通,永善', 3, 870, 657300, 'Yongshan', 'YS', 'Y', '103.63504', '28.2279', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530626, 530600, '绥江县', '中国,云南省,昭通市,绥江县', '绥江', '中国,云南,昭通,绥江', 3, 870, 657700, 'Suijiang', 'SJ', 'S', '103.94937', '28.59661', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530627, 530600, '镇雄县', '中国,云南省,昭通市,镇雄县', '镇雄', '中国,云南,昭通,镇雄', 3, 870, 657200, 'Zhenxiong', 'ZX', 'Z', '104.87258', '27.43981', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530628, 530600, '彝良县', '中国,云南省,昭通市,彝良县', '彝良', '中国,云南,昭通,彝良', 3, 870, 657600, 'Yiliang', 'YL', 'Y', '104.04983', '27.62809', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530629, 530600, '威信县', '中国,云南省,昭通市,威信县', '威信', '中国,云南,昭通,威信', 3, 870, 657900, 'Weixin', 'WX', 'W', '105.04754', '27.84065', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530630, 530600, '水富县', '中国,云南省,昭通市,水富县', '水富', '中国,云南,昭通,水富', 3, 870, 657800, 'Shuifu', 'SF', 'S', '104.4158', '28.62986', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530700, 530000, '丽江市', '中国,云南省,丽江市', '丽江', '中国,云南,丽江', 2, 888, 674100, 'Lijiang', 'LJ', 'L', '100.233026', '26.872108', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530702, 530700, '古城区', '中国,云南省,丽江市,古城区', '古城', '中国,云南,丽江,古城', 3, 888, 674100, 'Gucheng', 'GC', 'G', '100.2257', '26.87697', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530721, 530700, '玉龙纳西族自治县', '中国,云南省,丽江市,玉龙纳西族自治县', '玉龙', '中国,云南,丽江,玉龙', 3, 888, 674100, 'Yulong', 'YL', 'Y', '100.2369', '26.82149', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530722, 530700, '永胜县', '中国,云南省,丽江市,永胜县', '永胜', '中国,云南,丽江,永胜', 3, 888, 674200, 'Yongsheng', 'YS', 'Y', '100.74667', '26.68591', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530723, 530700, '华坪县', '中国,云南省,丽江市,华坪县', '华坪', '中国,云南,丽江,华坪', 3, 888, 674800, 'Huaping', 'HP', 'H', '101.26562', '26.62967', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530724, 530700, '宁蒗彝族自治县', '中国,云南省,丽江市,宁蒗彝族自治县', '宁蒗', '中国,云南,丽江,宁蒗', 3, 888, 674300, 'Ninglang', 'NL', 'N', '100.8507', '27.28179', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530800, 530000, '普洱市', '中国,云南省,普洱市', '普洱', '中国,云南,普洱', 2, 879, 665000, 'Pu\'er', 'PE', 'P', '100.972344', '22.777321', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530802, 530800, '思茅区', '中国,云南省,普洱市,思茅区', '思茅', '中国,云南,普洱,思茅', 3, 879, 665000, 'Simao', 'SM', 'S', '100.97716', '22.78691', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530821, 530800, '宁洱哈尼族彝族自治县', '中国,云南省,普洱市,宁洱哈尼族彝族自治县', '宁洱', '中国,云南,普洱,宁洱', 3, 879, 665100, 'Ninger', 'NE', 'N', '101.04653', '23.06341', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530822, 530800, '墨江哈尼族自治县', '中国,云南省,普洱市,墨江哈尼族自治县', '墨江', '中国,云南,普洱,墨江', 3, 879, 654800, 'Mojiang', 'MJ', 'M', '101.69171', '23.43214', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530823, 530800, '景东彝族自治县', '中国,云南省,普洱市,景东彝族自治县', '景东', '中国,云南,普洱,景东', 3, 879, 676200, 'Jingdong', 'JD', 'J', '100.83599', '24.44791', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530824, 530800, '景谷傣族彝族自治县', '中国,云南省,普洱市,景谷傣族彝族自治县', '景谷', '中国,云南,普洱,景谷', 3, 879, 666400, 'Jinggu', 'JG', 'J', '100.70251', '23.49705', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530825, 530800, '镇沅彝族哈尼族拉祜族自治县', '中国,云南省,普洱市,镇沅彝族哈尼族拉祜族自治县', '镇沅', '中国,云南,普洱,镇沅', 3, 879, 666500, 'Zhenyuan', 'ZY', 'Z', '101.10675', '24.00557', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530826, 530800, '江城哈尼族彝族自治县', '中国,云南省,普洱市,江城哈尼族彝族自治县', '江城', '中国,云南,普洱,江城', 3, 879, 665900, 'Jiangcheng', 'JC', 'J', '101.85788', '22.58424', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530827, 530800, '孟连傣族拉祜族佤族自治县', '中国,云南省,普洱市,孟连傣族拉祜族佤族自治县', '孟连', '中国,云南,普洱,孟连', 3, 879, 665800, 'Menglian', 'ML', 'M', '99.58424', '22.32922', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530828, 530800, '澜沧拉祜族自治县', '中国,云南省,普洱市,澜沧拉祜族自治县', '澜沧', '中国,云南,普洱,澜沧', 3, 879, 665600, 'Lancang', 'LC', 'L', '99.93591', '22.55474', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530829, 530800, '西盟佤族自治县', '中国,云南省,普洱市,西盟佤族自治县', '西盟', '中国,云南,普洱,西盟', 3, 879, 665700, 'Ximeng', 'XM', 'X', '99.59869', '22.64774', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530900, 530000, '临沧市', '中国,云南省,临沧市', '临沧', '中国,云南,临沧', 2, 883, 677000, 'Lincang', 'LC', 'L', '100.08697', '23.886567', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530902, 530900, '临翔区', '中国,云南省,临沧市,临翔区', '临翔', '中国,云南,临沧,临翔', 3, 883, 677000, 'Linxiang', 'LX', 'L', '100.08242', '23.89497', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530921, 530900, '凤庆县', '中国,云南省,临沧市,凤庆县', '凤庆', '中国,云南,临沧,凤庆', 3, 883, 675900, 'Fengqing', 'FQ', 'F', '99.92837', '24.58034', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530922, 530900, '云县', '中国,云南省,临沧市,云县', '云县', '中国,云南,临沧,云县', 3, 883, 675800, 'Yunxian', 'YX', 'Y', '100.12808', '24.44675', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530923, 530900, '永德县', '中国,云南省,临沧市,永德县', '永德', '中国,云南,临沧,永德', 3, 883, 677600, 'Yongde', 'YD', 'Y', '99.25326', '24.0276', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530924, 530900, '镇康县', '中国,云南省,临沧市,镇康县', '镇康', '中国,云南,临沧,镇康', 3, 883, 677700, 'Zhenkang', 'ZK', 'Z', '98.8255', '23.76241', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530925, 530900, '双江拉祜族佤族布朗族傣族自治县', '中国,云南省,临沧市,双江拉祜族佤族布朗族傣族自治县', '双江', '中国,云南,临沧,双江', 3, 883, 677300, 'Shuangjiang', 'SJ', 'S', '99.82769', '23.47349', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530926, 530900, '耿马傣族佤族自治县', '中国,云南省,临沧市,耿马傣族佤族自治县', '耿马', '中国,云南,临沧,耿马', 3, 883, 677500, 'Gengma', 'GM', 'G', '99.39785', '23.53776', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (530927, 530900, '沧源佤族自治县', '中国,云南省,临沧市,沧源佤族自治县', '沧源', '中国,云南,临沧,沧源', 3, 883, 677400, 'Cangyuan', 'CY', 'C', '99.24545', '23.14821', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532300, 530000, '楚雄彝族自治州', '中国,云南省,楚雄彝族自治州', '楚雄', '中国,云南,楚雄', 2, 878, 675000, 'Chuxiong', 'CX', 'C', '101.546046', '25.041988', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532301, 532300, '楚雄市', '中国,云南省,楚雄彝族自治州,楚雄市', '楚雄', '中国,云南,楚雄,楚雄', 3, 878, 675000, 'Chuxiong', 'CX', 'C', '101.54615', '25.0329', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532322, 532300, '双柏县', '中国,云南省,楚雄彝族自治州,双柏县', '双柏', '中国,云南,楚雄,双柏', 3, 878, 675100, 'Shuangbai', 'SB', 'S', '101.64205', '24.68882', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532323, 532300, '牟定县', '中国,云南省,楚雄彝族自治州,牟定县', '牟定', '中国,云南,楚雄,牟定', 3, 878, 675500, 'Mouding', 'MD', 'M', '101.54', '25.31551', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532324, 532300, '南华县', '中国,云南省,楚雄彝族自治州,南华县', '南华', '中国,云南,楚雄,南华', 3, 878, 675200, 'Nanhua', 'NH', 'N', '101.27313', '25.19293', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532325, 532300, '姚安县', '中国,云南省,楚雄彝族自治州,姚安县', '姚安', '中国,云南,楚雄,姚安', 3, 878, 675300, 'Yao\'an', 'YA', 'Y', '101.24279', '25.50467', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532326, 532300, '大姚县', '中国,云南省,楚雄彝族自治州,大姚县', '大姚', '中国,云南,楚雄,大姚', 3, 878, 675400, 'Dayao', 'DY', 'D', '101.32397', '25.72218', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532327, 532300, '永仁县', '中国,云南省,楚雄彝族自治州,永仁县', '永仁', '中国,云南,楚雄,永仁', 3, 878, 651400, 'Yongren', 'YR', 'Y', '101.6716', '26.05794', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532328, 532300, '元谋县', '中国,云南省,楚雄彝族自治州,元谋县', '元谋', '中国,云南,楚雄,元谋', 3, 878, 651300, 'Yuanmou', 'YM', 'Y', '101.87728', '25.70438', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532329, 532300, '武定县', '中国,云南省,楚雄彝族自治州,武定县', '武定', '中国,云南,楚雄,武定', 3, 878, 651600, 'Wuding', 'WD', 'W', '102.4038', '25.5295', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532331, 532300, '禄丰县', '中国,云南省,楚雄彝族自治州,禄丰县', '禄丰', '中国,云南,楚雄,禄丰', 3, 878, 651200, 'Lufeng', 'LF', 'L', '102.07797', '25.14815', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532500, 530000, '红河哈尼族彝族自治州', '中国,云南省,红河哈尼族彝族自治州', '红河', '中国,云南,红河', 2, 873, 661400, 'Honghe', 'HH', 'H', '103.384182', '23.366775', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532501, 532500, '个旧市', '中国,云南省,红河哈尼族彝族自治州,个旧市', '个旧', '中国,云南,红河,个旧', 3, 873, 661000, 'Gejiu', 'GJ', 'G', '103.15966', '23.35894', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532502, 532500, '开远市', '中国,云南省,红河哈尼族彝族自治州,开远市', '开远', '中国,云南,红河,开远', 3, 873, 661600, 'Kaiyuan', 'KY', 'K', '103.26986', '23.71012', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532503, 532500, '蒙自市', '中国,云南省,红河哈尼族彝族自治州,蒙自市', '蒙自', '中国,云南,红河,蒙自', 3, 873, 661101, 'Mengzi', 'MZ', 'M', '103.385005', '23.366843', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532504, 532500, '弥勒市', '中国,云南省,红河哈尼族彝族自治州,弥勒市', '弥勒', '中国,云南,红河,弥勒', 3, 873, 652300, 'Mile ', 'ML', 'M', '103.436988', '24.40837', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532523, 532500, '屏边苗族自治县', '中国,云南省,红河哈尼族彝族自治州,屏边苗族自治县', '屏边', '中国,云南,红河,屏边', 3, 873, 661200, 'Pingbian', 'PB', 'P', '103.68554', '22.98425', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532524, 532500, '建水县', '中国,云南省,红河哈尼族彝族自治州,建水县', '建水', '中国,云南,红河,建水', 3, 873, 654300, 'Jianshui', 'JS', 'J', '102.82656', '23.63472', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532525, 532500, '石屏县', '中国,云南省,红河哈尼族彝族自治州,石屏县', '石屏', '中国,云南,红河,石屏', 3, 873, 662200, 'Shiping', 'SP', 'S', '102.49408', '23.71441', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532527, 532500, '泸西县', '中国,云南省,红河哈尼族彝族自治州,泸西县', '泸西', '中国,云南,红河,泸西', 3, 873, 652400, 'Luxi', 'LX', 'L', '103.76373', '24.52854', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532528, 532500, '元阳县', '中国,云南省,红河哈尼族彝族自治州,元阳县', '元阳', '中国,云南,红河,元阳', 3, 873, 662400, 'Yuanyang', 'YY', 'Y', '102.83261', '23.22281', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532529, 532500, '红河县', '中国,云南省,红河哈尼族彝族自治州,红河县', '红河县', '中国,云南,红河,红河县', 3, 873, 654400, 'Honghexian', 'HHX', 'H', '102.42059', '23.36767', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532530, 532500, '金平苗族瑶族傣族自治县', '中国,云南省,红河哈尼族彝族自治州,金平苗族瑶族傣族自治县', '金平', '中国,云南,红河,金平', 3, 873, 661500, 'Jinping', 'JP', 'J', '103.22651', '22.77959', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532531, 532500, '绿春县', '中国,云南省,红河哈尼族彝族自治州,绿春县', '绿春', '中国,云南,红河,绿春', 3, 873, 662500, 'Lvchun', 'LC', 'L', '102.39672', '22.99371', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532532, 532500, '河口瑶族自治县', '中国,云南省,红河哈尼族彝族自治州,河口瑶族自治县', '河口', '中国,云南,红河,河口', 3, 873, 661300, 'Hekou', 'HK', 'H', '103.93936', '22.52929', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532600, 530000, '文山壮族苗族自治州', '中国,云南省,文山壮族苗族自治州', '文山', '中国,云南,文山', 2, 876, 663000, 'Wenshan', 'WS', 'W', '104.24401', '23.36951', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532601, 532600, '文山市', '中国,云南省,文山壮族苗族自治州,文山市', '文山', '中国,云南,文山,文山', 3, 876, 663000, 'Wenshan', 'WS', 'W', '104.244277', '23.369216', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532622, 532600, '砚山县', '中国,云南省,文山壮族苗族自治州,砚山县', '砚山', '中国,云南,文山,砚山', 3, 876, 663100, 'Yanshan', 'YS', 'Y', '104.33306', '23.60723', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532623, 532600, '西畴县', '中国,云南省,文山壮族苗族自治州,西畴县', '西畴', '中国,云南,文山,西畴', 3, 876, 663500, 'Xichou', 'XC', 'X', '104.67419', '23.43941', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532624, 532600, '麻栗坡县', '中国,云南省,文山壮族苗族自治州,麻栗坡县', '麻栗坡', '中国,云南,文山,麻栗坡', 3, 876, 663600, 'Malipo', 'MLP', 'M', '104.70132', '23.12028', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532625, 532600, '马关县', '中国,云南省,文山壮族苗族自治州,马关县', '马关', '中国,云南,文山,马关', 3, 876, 663700, 'Maguan', 'MG', 'M', '104.39514', '23.01293', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532626, 532600, '丘北县', '中国,云南省,文山壮族苗族自治州,丘北县', '丘北', '中国,云南,文山,丘北', 3, 876, 663200, 'Qiubei', 'QB', 'Q', '104.19256', '24.03957', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532627, 532600, '广南县', '中国,云南省,文山壮族苗族自治州,广南县', '广南', '中国,云南,文山,广南', 3, 876, 663300, 'Guangnan', 'GN', 'G', '105.05511', '24.0464', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532628, 532600, '富宁县', '中国,云南省,文山壮族苗族自治州,富宁县', '富宁', '中国,云南,文山,富宁', 3, 876, 663400, 'Funing', 'FN', 'F', '105.63085', '23.62536', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532800, 530000, '西双版纳傣族自治州', '中国,云南省,西双版纳傣族自治州', '西双版纳', '中国,云南,西双版纳', 2, 691, 666100, 'Xishuangbanna', 'XSBN', 'X', '100.797941', '22.001724', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532801, 532800, '景洪市', '中国,云南省,西双版纳傣族自治州,景洪市', '景洪', '中国,云南,西双版纳,景洪', 3, 691, 666100, 'Jinghong', 'JH', 'J', '100.79977', '22.01071', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532822, 532800, '勐海县', '中国,云南省,西双版纳傣族自治州,勐海县', '勐海', '中国,云南,西双版纳,勐海', 3, 691, 666200, 'Menghai', 'MH', 'M', '100.44931', '21.96175', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532823, 532800, '勐腊县', '中国,云南省,西双版纳傣族自治州,勐腊县', '勐腊', '中国,云南,西双版纳,勐腊', 3, 691, 666300, 'Mengla', 'ML', 'M', '101.56488', '21.48162', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532900, 530000, '大理白族自治州', '中国,云南省,大理白族自治州', '大理', '中国,云南,大理', 2, 872, 671000, 'Dali', 'DL', 'D', '100.240037', '25.592765', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532901, 532900, '大理市', '中国,云南省,大理白族自治州,大理市', '大理', '中国,云南,大理,大理', 3, 872, 671000, 'Dali', 'DL', 'D', '100.22998', '25.59157', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532922, 532900, '漾濞彝族自治县', '中国,云南省,大理白族自治州,漾濞彝族自治县', '漾濞', '中国,云南,大理,漾濞', 3, 872, 672500, 'Yangbi', 'YB', 'Y', '99.95474', '25.6652', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532923, 532900, '祥云县', '中国,云南省,大理白族自治州,祥云县', '祥云', '中国,云南,大理,祥云', 3, 872, 672100, 'Xiangyun', 'XY', 'X', '100.55761', '25.47342', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532924, 532900, '宾川县', '中国,云南省,大理白族自治州,宾川县', '宾川', '中国,云南,大理,宾川', 3, 872, 671600, 'Binchuan', 'BC', 'B', '100.57666', '25.83144', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532925, 532900, '弥渡县', '中国,云南省,大理白族自治州,弥渡县', '弥渡', '中国,云南,大理,弥渡', 3, 872, 675600, 'Midu', 'MD', 'M', '100.49075', '25.34179', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532926, 532900, '南涧彝族自治县', '中国,云南省,大理白族自治州,南涧彝族自治县', '南涧', '中国,云南,大理,南涧', 3, 872, 675700, 'Nanjian', 'NJ', 'N', '100.50964', '25.04349', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532927, 532900, '巍山彝族回族自治县', '中国,云南省,大理白族自治州,巍山彝族回族自治县', '巍山', '中国,云南,大理,巍山', 3, 872, 672400, 'Weishan', 'WS', 'W', '100.30612', '25.23197', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532928, 532900, '永平县', '中国,云南省,大理白族自治州,永平县', '永平', '中国,云南,大理,永平', 3, 872, 672600, 'Yongping', 'YP', 'Y', '99.54095', '25.46451', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532929, 532900, '云龙县', '中国,云南省,大理白族自治州,云龙县', '云龙', '中国,云南,大理,云龙', 3, 872, 672700, 'Yunlong', 'YL', 'Y', '99.37255', '25.88505', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532930, 532900, '洱源县', '中国,云南省,大理白族自治州,洱源县', '洱源', '中国,云南,大理,洱源', 3, 872, 671200, 'Eryuan', 'EY', 'E', '99.94903', '26.10829', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532931, 532900, '剑川县', '中国,云南省,大理白族自治州,剑川县', '剑川', '中国,云南,大理,剑川', 3, 872, 671300, 'Jianchuan', 'JC', 'J', '99.90545', '26.53688', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (532932, 532900, '鹤庆县', '中国,云南省,大理白族自治州,鹤庆县', '鹤庆', '中国,云南,大理,鹤庆', 3, 872, 671500, 'Heqing', 'HQ', 'H', '100.17697', '26.55798', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (533100, 530000, '德宏傣族景颇族自治州', '中国,云南省,德宏傣族景颇族自治州', '德宏', '中国,云南,德宏', 2, 692, 678400, 'Dehong', 'DH', 'D', '98.578363', '24.436694', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (533102, 533100, '瑞丽市', '中国,云南省,德宏傣族景颇族自治州,瑞丽市', '瑞丽', '中国,云南,德宏,瑞丽', 3, 692, 678600, 'Ruili', 'RL', 'R', '97.85183', '24.01277', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (533103, 533100, '芒市', '中国,云南省,德宏傣族景颇族自治州,芒市', '芒市', '中国,云南,德宏,芒市', 3, 692, 678400, 'Mangshi', 'MS', 'M', '98.588641', '24.433735', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (533122, 533100, '梁河县', '中国,云南省,德宏傣族景颇族自治州,梁河县', '梁河', '中国,云南,德宏,梁河', 3, 692, 679200, 'Lianghe', 'LH', 'L', '98.29705', '24.80658', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (533123, 533100, '盈江县', '中国,云南省,德宏傣族景颇族自治州,盈江县', '盈江', '中国,云南,德宏,盈江', 3, 692, 679300, 'Yingjiang', 'YJ', 'Y', '97.93179', '24.70579', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (533124, 533100, '陇川县', '中国,云南省,德宏傣族景颇族自治州,陇川县', '陇川', '中国,云南,德宏,陇川', 3, 692, 678700, 'Longchuan', 'LC', 'L', '97.79199', '24.18302', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (533300, 530000, '怒江傈僳族自治州', '中国,云南省,怒江傈僳族自治州', '怒江', '中国,云南,怒江', 2, 886, 673100, 'Nujiang', 'NJ', 'N', '98.854304', '25.850949', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (533301, 533300, '泸水市', '中国,云南省,怒江傈僳族自治州,泸水市', '泸水', '中国,云南,怒江,泸水', 3, 886, 673200, 'Lushui', 'LS', 'L', '98.85534', '25.83772', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (533323, 533300, '福贡县', '中国,云南省,怒江傈僳族自治州,福贡县', '福贡', '中国,云南,怒江,福贡', 3, 886, 673400, 'Fugong', 'FG', 'F', '98.86969', '26.90366', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (533324, 533300, '贡山独龙族怒族自治县', '中国,云南省,怒江傈僳族自治州,贡山独龙族怒族自治县', '贡山', '中国,云南,怒江,贡山', 3, 886, 673500, 'Gongshan', 'GS', 'G', '98.66583', '27.74088', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (533325, 533300, '兰坪白族普米族自治县', '中国,云南省,怒江傈僳族自治州,兰坪白族普米族自治县', '兰坪', '中国,云南,怒江,兰坪', 3, 886, 671400, 'Lanping', 'LP', 'L', '99.41891', '26.45251', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (533400, 530000, '迪庆藏族自治州', '中国,云南省,迪庆藏族自治州', '迪庆', '中国,云南,迪庆', 2, 887, 674400, 'Deqen', 'DQ', 'D', '99.706463', '27.826853', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (533401, 533400, '香格里拉市', '中国,云南省,迪庆藏族自治州,香格里拉市', '香格里拉', '中国,云南,迪庆,香格里拉', 3, 887, 674400, 'Xianggelila', 'XGLL', 'X', '99.70601', '27.82308', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (533422, 533400, '德钦县', '中国,云南省,迪庆藏族自治州,德钦县', '德钦', '中国,云南,迪庆,德钦', 3, 887, 674500, 'Deqin', 'DQ', 'D', '98.91082', '28.4863', NULL, NULL, NULL);
INSERT INTO `ann_area` VALUES (533423, 533400, '维西傈僳族自治县', '中国,云南省,迪庆藏族自治州,维西傈僳族自治县', '维西', '中国,云南,迪庆,维西', 3, 887, 674600, 'Weixi', 'WX', 'W', '99.28402', '27.1793', NULL, NULL, NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
